package com.example.projectbandera_Firebase;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.example.CountriesProject.R;
import com.example.CountriesProject.databinding.ActivityFirebaseBinding;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class firebaseActivity extends AppCompatActivity {
    private ActivityFirebaseBinding binding;
    private FirebaseFirestore db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_firebase);
        binding = ActivityFirebaseBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);
        db = FirebaseFirestore.getInstance();

    }
    /*public void onClickCrearBandera(View view){
        Map<String, Object> countrie=new HashMap<>();
        countrie.put("Nombre: ", "Argelia");
        countrie.put("Capital","Argel");
        countrie.put("Poblacion","43851043");
        countrie.put("bandera","\"https://i.pinimg.com/originals/27/50/85/27508519573a86adb9142ecda911a1dd.png");
        countrie.put("himno","<iframe width=\"681\" height=\"511\" src=\"https://youtube.com/embed/sLy650hukPc\" frameborder=\"0\" allow=\"accelerometer; autoplay; +" +
                "encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>");
        db.collection("countries")
                .add(countrie)
                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {
                        Log.d("FB","Pais agregado correctamente"+documentReference.getId());

                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w("FB", "El pais no se pudo agregar",e);
                    }
                });
    }

*/

    public void onClickConsultarProductos(View view) {
        db.collection("Pais")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Log.d("FB", document.getId() +  "-->" + document.getData());
                            }
                        }
                    }
                });
    }
}
