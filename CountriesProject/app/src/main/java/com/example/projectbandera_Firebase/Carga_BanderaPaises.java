package com.example.projectbandera_Firebase;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import com.example.CountriesProject.MainActivity;
import com.example.CountriesProject.adatador.AdapterPais;
import com.example.CountriesProject.databinding.ActivityFirebaseBinding;
import com.example.CountriesProject.databinding.ActivityMainBinding;
import com.example.CountriesProject.modelView.Pais;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Carga_BanderaPaises extends AppCompatActivity {
    private ActivityMainBinding binding;
    private FirebaseFirestore db;
    private MainActivity main =new MainActivity();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding =ActivityMainBinding.inflate(getLayoutInflater());
        View view =binding.getRoot();
        setContentView(view);
        db= FirebaseFirestore.getInstance() ;


        consultarLisBandera();
    }

    /*public void cargarDatosFirebase(View view){

        List<Pais> paises = main.datos();
        for (Pais pais:paises){
            Map<String,Object> mapa = getMapa(pais);
             db.collection("Paises")
                     .add(mapa)
                     .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                         @Override
                         public void onSuccess(DocumentReference documentReference) {
                             Log.d("FB","El pais fue ingresado correctamente" + documentReference.getId());
                         }
                     })
                     .addOnFailureListener(new OnFailureListener() {
                         @Override
                         public void onFailure(@NonNull Exception e) {
                             Log.w("FB","Error el pasis no agregado con sus datos correspondientes",e);
                         }
                     });

         }

    }

    private Map<String,Object>  getMapa(Pais paisBandera) {
        Map<String,Object> mapa = new HashMap<>();
        mapa.put("Nombre",paisBandera.getNombre());
        mapa.put("Capital",paisBandera.getCapital());
        mapa.put("Poblacion",paisBandera.getPoblacion());
        mapa.put("Bandera",paisBandera.getUrlBandera());
        mapa.put("Himno",paisBandera.getUrlHimno());
        return mapa;
    }*/

    private void consultarLisBandera() {
        db.collection("Paises")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            List<Pais> pais = new ArrayList<>();
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Map<String,Object> md = document.getData();
                                //int  = ((Long) md.get("codigo")).intValue();
                                String nombre = (String) md.get("Nombre");
                                String capital= (String) md.get("Capital");
                                String poblacion = (String) md.get("Poblacion");
                                String urlBandera = (String)md.get("URL ");
                                String urlHimno = (String)md.get("URL ");
                                pais.add(new Pais(nombre,capital,poblacion,urlBandera,urlHimno));
                            }


                            //pais = task.getResult().toObjects(Pais.class);
                            cargarRecyclerView(pais);
                        }
                    }
                });
    }


    private void cargarRecyclerView(List<Pais> pais) {
        binding.recyclerViewPais.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        binding.recyclerViewPais.setLayoutManager(layoutManager);
        RecyclerView.Adapter mAdapter = new AdapterPais(pais);
        binding.recyclerViewPais.setAdapter(mAdapter);



    }


}







